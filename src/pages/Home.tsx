import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonList,
  IonItem,
  IonLabel,
  IonListHeader,
  IonText,
  IonFooter,
  IonButton,
  IonRange,
} from '@ionic/react';
import React from 'react';
import { FcmHelper, Notification } from 'ijooz-fcm';
import { FileTransfer, FileUploadOptions } from '@ionic-native/file-transfer';
import { Chooser } from '@ionic-native/chooser';
import { Camera, CameraOptions } from '@ionic-native/camera';

const INITIAL_STATE = {
  notifications: [
    {
      id: 'id',
      title: 'Sample Message',
      body: 'This is a sample push notification message',
      data: {},
    },
  ],
};

const baseUrl = `https://api.ijooz.com/api/v1/files`;
// const baseUrl = 'https://10.0.2.2:4400/v1/files';
// const baseUrl = 'https://10.0.2.2:6701';
// const baseUrl = 'https://api.ijooz.com/test';

class FileInfo {
  uri: string;
  name: string;
  mediaType: string;

  constructor(uri: string, name: string, mediaType: string) {
    this.uri = uri;
    this.name = name;
    this.mediaType = mediaType;
  }
}

export class Home extends React.Component {
  state: any = {};
  props: any = {};
  constructor(props: any) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  fcmHelper = new FcmHelper();

  componentDidMount() {
    this.fcmHelper
      .subscribeSuccessfulRegistration(
        this.onPushRegistrationSuccess.bind(this)
      )
      .subscribeErrorRegistration(this.onPushRegistrationError.bind(this))
      .subscribeForegroundNotificationReceived(
        this.onForegroundNotificationReceived.bind(this)
      )
      .subscribeBackgroundNotificationClicked(
        this.onBackgroundNotificationClicked.bind(this),
        true
      )
      .init();
  }

  componentWillUnmount() {
    console.log(`Disposing FCM Helper`);
    this.fcmHelper.dispose();
  }

  onPushRegistrationSuccess(token: string) {
    console.log(`Successful registration: ${token}`);
  }

  onForegroundNotificationReceived(notification: Notification) {
    console.log(`Push received: ${JSON.stringify(notification)}`);
    let notif = this.state.notifications;
    notif.push(notification);
    this.setState({
      notifications: notif,
    });
  }

  onBackgroundNotificationClicked(notification: Notification) {
    console.log(`Push action performed: ${JSON.stringify(notification)}`);
    let notif = this.state.notifications;
    notif.push(notification);
    this.setState({
      notifications: notif,
    });
  }

  onPushRegistrationError(error: object) {
    alert(`Error on registration: ${JSON.stringify(error)}`);
  }

  getToken() {
    const token = this.fcmHelper.getToken();
    console.log(`FCM token: ${token}`);
    alert(`FCM token: ${token}`);
  }

  async upload() {
    try {
      const selectedFile = await this.getFile();
      if (!selectedFile) {
        alert('No file chosen');
        return;
      }

      await this.uploadFile(`${baseUrl}/save?folder=abc`, selectedFile);
      // await this.uploadFile(`${baseUrl}/uploadToDrive`, selectedFile);
    } catch (error) {
      console.error(error);
    }
  }

  async uploadToDb() {
    try {
      const selectedFile = await this.getFile();
      if (!selectedFile) {
        alert('No file chosen');
        return;
      }

      await this.uploadFile(`${baseUrl}/upload3`, selectedFile);
    } catch (error) {
      console.error(error);
    }
  }

  async getFile(): Promise<FileInfo | undefined> {
    try {
      const chooserResult = await Chooser.getFile();
      if (!chooserResult) {
        return undefined;
      }

      const { uri, name, mediaType } = chooserResult;
      console.log(`Selected: ${JSON.stringify({ uri, name, mediaType })}`);
      return new FileInfo(uri, name, mediaType);
    } catch (error) {
      console.error(error);
    }
  }

  async uploadFile(uploadUri: string, fileInfo: FileInfo) {
    const token =
      'c47be68400dc3cf62e43d6a214d9cfaa6db1adbcfeb89f4b18c0ea4572c9076c';

    const options: FileUploadOptions = {
      fileKey: 'file',
      fileName: fileInfo.name,
      mimeType: fileInfo.mediaType,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    const result = await FileTransfer.create().upload(
      fileInfo.uri,
      encodeURI(uploadUri),
      options
    );
    console.dir(result);
  }

  async snapPhoto() {
    const cameraOptions: CameraOptions = {
      quality: this.quality,
      destinationType: Camera.DestinationType.FILE_URI,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      targetWidth: 1024,
      targetHeight: 1024,
    };
    console.log(`Selected quality: ${this.quality}`);

    try {
      const fileUri = await Camera.getPicture(cameraOptions);
      if (!fileUri) {
        alert('No photo captured');
        return;
      }

      const fileInfo = new FileInfo(fileUri, `${Date.now()}.jpg`, 'image/jpeg');
      await this.uploadFile(`${baseUrl}/save?folder=aaa`, fileInfo);
      // await this.uploadFile(`${baseUrl}/uploadToDrive`, fileInfo);
    } catch (error) {
      console.error(error);
    }
  }

  quality = 100;
  setQuality(quality: number) {
    this.quality = quality;
  }

  render() {
    const { notifications } = this.state;
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar color="primary">
            <IonTitle>Enappd</IonTitle>
          </IonToolbar>
          <IonToolbar color="medium">
            <IonTitle>Ionic React Push Example</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding">
          <IonList>
            <IonListHeader>
              <IonLabel>Notifications</IonLabel>
            </IonListHeader>
            {notifications &&
              notifications.map((notif: any) => (
                <IonItem key={notif.id}>
                  <IonLabel>
                    <IonText>
                      <h3>{notif.title}</h3>
                    </IonText>
                    <p>{notif.body}</p>
                    <p>{JSON.stringify(notif.data)}</p>
                  </IonLabel>
                </IonItem>
              ))}
          </IonList>
        </IonContent>
        <IonFooter>
          <IonToolbar>
            <IonItem>
              <IonButton onClick={() => this.getToken()}>
                Show FCM Token
              </IonButton>
            </IonItem>
            <IonItem>
              <IonButton onClick={() => this.upload()}>Upload File</IonButton>
              <IonButton onClick={() => this.uploadToDb()}>
                Upload File To DB
              </IonButton>
            </IonItem>
            <IonItem>
              <IonLabel>Quality</IonLabel>
              <IonRange
                min={10}
                max={100}
                step={5}
                snaps={true}
                pin={true}
                color="secondary"
                onIonChange={(e) => this.setQuality(e.detail.value as number)}
              >
                <IonLabel slot="start">10</IonLabel>
                <IonLabel slot="end">100</IonLabel>
              </IonRange>
            </IonItem>
            <IonItem>
              <IonButton onClick={() => this.snapPhoto()}>
                Snap and Upload Photo
              </IonButton>
            </IonItem>
          </IonToolbar>
        </IonFooter>
      </IonPage>
    );
  }
}
export default Home;
