#!/usr/bin/env bash

# fail if any command fails
set -e
# debug log
set -x

# Required nodeJS version
NODE_VERSION=12.18.3

# workaround to override the v8 alias
npm config delete prefix

npm config set "//se27t1pt.bytesafe.dev/r/:_authToken" $NPM_AUTH_TOKEN
npm config set registry https://se27t1pt.bytesafe.dev/r/default

. ~/.bashrc
nvm install "$NODE_VERSION"
nvm alias node12 "$NODE_VERSION"

# go to root of project
cd ../..

# install dependencies
npm i

npm config delete "//se27t1pt.bytesafe.dev/r/:_authToken"

# run optimized production build
npm run build -- --prod

# copy the web assets to the native projects and updates the native plugins and dependencies based in package.json
npx cap sync